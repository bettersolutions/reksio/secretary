package dac.reksio.secretary.s3.forward.dlt;

import dac.reksio.secretary.files.FileEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DltHashProvider {

    private final DltClient dltClient;

    public DltHashDto getHashOfFile(FileEntity file) {
        return dltClient.getHashOfFile(file.getFilename());
    }
}
